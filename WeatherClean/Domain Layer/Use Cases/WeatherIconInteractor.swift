//
//  WeatherIconInteractor.swift
//  WeatherClean
//
//  Created by Luka on 15/05/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

class WeatherIconInteractor: WeatherIconUseCase {
    let provider: WeatherIconProvider
    
    init(provider: WeatherIconProvider) {
        self.provider = provider
    }
    
    func fetchIcon(for path: String, completion: @escaping (Response<WeatherIcon>) -> Void) {
        provider.fetchIcon(for: path, completion: {response in
            switch response{
            case .success(let iconData):
                completion(.success(WeatherIcon(path: path, image: iconData)))
            case .error(let error):
                print(error.localizedDescription)
            }
        })
    }
}

