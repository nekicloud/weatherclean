//
//  WeatherInteractor.swift
//  WeatherClean
//
//  Created by Luka on 15/05/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

class WeatherInteractor: WeatherUseCase{
    let provider: WeatherProvider
    
    init(provider: WeatherProvider) {
        self.provider = provider
    }
    func fetchWeatherList(lat: Double, lon: Double, completion: @escaping (Response<WeatherList>) -> Void) {
        provider.fetchWeather(lat: lat, lon: lon, completion: completion)
    }
}


