//
//  LocationUseCase.swift
//  WeatherClean
//
//  Created by Luka on 03/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

protocol CoordinatesUseCase {
    func fetchCoordinates() -> Coordinates?
}
