//
//  LocationInteractor.swift
//  WeatherClean
//
//  Created by Luka on 03/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

class CoordinatesInteractor: CoordinatesUseCase {
    let provider:CoordinatesProvider
    
    init(provider: CoordinatesProvider){
        self.provider = provider
    }
    
    func fetchCoordinates() -> Coordinates? {
        return provider.fetchCoordinates()
    }
}
