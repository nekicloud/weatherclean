//
//  Response.swift
//  WeatherClean
//
//  Created by Luka on 15/05/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

enum Response<T> {
    case success(T)
    case error(Error)
}
