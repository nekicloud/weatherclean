//
//  WeatherIconProvider.swift
//  WeatherClean
//
//  Created by Luka on 15/05/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

protocol WeatherIconProvider {
    func fetchIcon(for path: String, completion: @escaping (Response<Data>) -> Void)
}
