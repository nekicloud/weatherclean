//
//  WeatherProvider.swift
//  WeatherClean
//
//  Created by Luka on 15/05/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

protocol WeatherProvider{
    func fetchWeather(lat: Double, lon: Double, completion: @escaping(Response<WeatherList>) -> Void)
}
