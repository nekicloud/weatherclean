//
//  Weather.swift
//  WeatherClean
//
//  Created by Luka on 15/05/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

protocol Weather {
    var city: String {get}
    var date: Date{get }
    var iconPath: String{get}
    var temperature : Double{get }
    var description: String{get}
    var humidity: Int {get }
    var precipitation: Double {get }
    var windSpeed: Double{get }
    var pressure: Double{get }
    var windDirection: Double{get }
}

typealias WeatherList = [Weather]
