//
//  LocationProvider.swift
//  WeatherClean
//
//  Created by Luka on 03/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import CoreLocation

class LocationProvider: CoordinatesProvider {
    var locationManager = CLLocationManager()
    
    func fetchCoordinates() -> Coordinates? {
        startUpdating()
        guard CLLocationManager.locationServicesEnabled() else {
            print("LOCATION NOT AUTHORIZED")
            return nil
        }
        return Coordinates(lat: (locationManager.location?.coordinate.latitude)!, lon: (locationManager.location?.coordinate.longitude)!)
    }
    
    private func startUpdating(){
        locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers
        locationManager.startUpdatingLocation()
    }
}
