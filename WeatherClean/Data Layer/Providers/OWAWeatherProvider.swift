//
//  OWAWeatherProvider.swift
//  WeatherClean
//
//  Created by Luka on 15/05/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

class OWAWeatherProvider: WeatherProvider{
    let webService: WebService
    
    init(webService: WebService = WebServiceProvider(session: DataNetworksession())){
        self.webService = webService
    }
    
    func fetchWeather(lat: Double, lon: Double, completion: @escaping (Response<WeatherList>) -> Void) {
        guard let request = ApiRequest(endpoint: .weatherFetch(lat: lat, lon: lon)).urlRequest else { return}
        webService.execute(request) { (response: Response<Result>) in
            switch response{
            case .success(let weatherList):
                completion(.success(getWeatherList(result: weatherList)))
            case .error(let error):
                print(error)
            }
        }
    }
}

private func getWeatherList(result: Result) -> WeatherList{
    var weatherList = WeatherList()
    for object in result.list{
        let nextWeather = OWAWeather(city: result.city.name, iconPath: object.currentWeather.icon, date: object.date, temperature: object.main.temp, description: object.currentWeather.description, humidity: object.main.humidity, precipitation: object.rain?.precipitation ?? 0, windSpeed: object.wind.speed ?? 0, pressure: object.main.pressure, windDirection: object.wind.deg ?? 0)
        weatherList.append(nextWeather)
    }
    return weatherList
}

struct OWAWeather: Weather{
    var city: String
    var iconPath: String
    var date: Date
    var temperature: Double
    var description: String
    var humidity: Int
    var precipitation: Double
    var windSpeed: Double
    var pressure: Double
    var windDirection: Double
    
    init(city: String, iconPath: String, date: Date, temperature: Double, description: String, humidity: Int, precipitation: Double, windSpeed: Double, pressure: Double, windDirection: Double){
        self.city = city
        self.iconPath = iconPath
        self.date = date
        self.temperature = temperature
        self.description = description
        self.humidity = humidity
        self.precipitation = precipitation
        self.windSpeed = windSpeed
        self.pressure = pressure
        self.windDirection = windDirection
    }
}

struct Result: Decodable {
    enum CodingKeys: String, CodingKey{
        case list
        case city
    }
    let list:[List]
    let city: City
}

struct City: Decodable {
    let name: String
}

struct List: Decodable {
    enum CodingKeys: String, CodingKey{
        case main, weather, wind, rain
        case dtTxt = "dt_txt"
    }
    let main: Main
    let weather: [ThisWeather]
    let wind: Wind
    let rain: Rain?
    let dtTxt: String
    var currentWeather: ThisWeather{
        return weather.first!
    }
    var date: Date {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        formatter.locale = Locale(identifier: "en_US_POSIX")
        return formatter.date(from: dtTxt)!
    }
}

struct Wind: Decodable{
    let speed, deg: Double?
}

struct Rain: Decodable{
    let precipitation: Double?
    enum CodingKeys: String, CodingKey{
        case precipitation = "3h"
    }
}

struct ThisWeather: Decodable{
    let description, icon: String
}

struct Main: Decodable {
    enum CodingKeys: String, CodingKey{
        case temp
        case pressure
        case humidity
    }
    let temp: Double
    let pressure: Double
    let humidity: Int
}


