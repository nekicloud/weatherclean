//
//  ApiRequest.swift
//  WeatherClean
//
//  Created by Luka on 15/05/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

struct ApiRequest{
    let baseUrlString = "https://api.openweathermap.org/data/2.5/forecast"
    let apiKey = "?APPID=74d05e65fd9872ac7d9444e7b1f5a09f"
    let endpoint: Endpoint
    
    init(endpoint: Endpoint) {
        self.endpoint = endpoint
    }
    
    var urlRequest: URLRequest? {
        let urlString = baseUrlString + apiKey + endpoint.parameters
        guard let url = URL(string: urlString) else { return nil}
        return URLRequest(url: url)
    }
}

enum RequestError: Error {
    case urlRequestFailed
}
