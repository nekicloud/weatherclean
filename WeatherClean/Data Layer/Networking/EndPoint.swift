//
//  EndPoint.swift
//  WeatherClean
//
//  Created by Luka on 15/05/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

enum Endpoint {
    case weatherFetch(lat: Double, lon: Double)
    
    var parameters: String{
        switch self{
        case let .weatherFetch(lat: lat, lon: lon):
            return "&lat=\(lat)&lon=\(lon)"
        }
    }
}
