//
//  WeatherIconRequest.swift
//  WeatherClean
//
//  Created by Luka on 15/05/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

class WeatherIconRequest {
    let baseUrl = "https://openweathermap.org/img/w/"
    let imagePath: String
    
    init(path: String) {
        imagePath = path
    }
    
    var urlRequest: URLRequest? {
        let urlString = baseUrl + imagePath + ".png"
        guard let url = URL(string: urlString) else {return nil}
        return URLRequest(url: url)
    }
}
