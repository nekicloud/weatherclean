//
//  TodayViewController.swift
//  WeatherClean
//
//  Created by Luka on 15/05/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit
import CoreLocation

class TodayViewController: UIViewController{
    @IBOutlet weak var weatherImg: UIImageView!
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var weatherLbl: UILabel!
    @IBOutlet weak var humidity: UILabel!
    @IBOutlet weak var precipitation: UILabel!
    @IBOutlet weak var pressure: UILabel!
    @IBOutlet weak var windSpd: UILabel!
    @IBOutlet weak var compass: UILabel!
    
    let dataSource: TodayViewControllerDataSource
    
    init(dataSource:TodayViewControllerDataSource){
        self.dataSource = dataSource
        super.init(nibName: nil, bundle: nil)
        dataSource.feedback = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        return nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        dataSource.fetchLocation()
        dataSource.fetchData()
    }
    
    private func getCelsius(kelvin: Double) -> String{
        let celsius = String(format: "%.0f", ((kelvin)-273.15).rounded()) + "°C | "
        return celsius
    }
    
    private func getWindDirection(degrees: Double) -> String{
        let directions = ["N", "NNE", "NE", "ENE", "E", "ESE", "SE", "SSE", "S", "SSW", "SW", "WSW", "W", "WNW", "NW", "NNW"]
        let i = Int((degrees+11.25)/22.5)
        return directions[i%16]
    }
    
    @IBAction func sharePressed(_ sender: Any) {
        let activityVC = UIActivityViewController(activityItems: ["I am in: " + self.locationLbl.text! + " the temperature is:" + self.weatherLbl.text!.prefix(4)], applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = self.view
        self.present(activityVC, animated: true, completion: nil)
        
    }
}

extension TodayViewController: TodayVCDataSourceUpdateFeedback {
    func dataSourceUpdated(weather: Weather) {
        locationLbl.text = weather.city
        weatherLbl.text = getCelsius(kelvin: weather.temperature) + weather.description
        humidity.text = String(weather.humidity) + "%"
        precipitation.text = String(weather.precipitation) + "mm"
        windSpd.text = String(format: "%.1f", (weather.windSpeed)*18/5) + "km/h"
        pressure.text = String(format:"%.0f",weather.pressure.rounded()) + " hPa"
        compass.text = getWindDirection(degrees: weather.windDirection)
    }
    
    func dataSourceImageFetched(image: UIImage) {
        weatherImg.image = image
    }
}
