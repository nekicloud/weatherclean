//
//  TodayViewControllerDataSource.swift
//  WeatherClean
//
//  Created by Luka on 17/05/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit

protocol TodayVCDataSourceUpdateFeedback: class {
    func dataSourceUpdated(weather: Weather)
    func dataSourceImageFetched(image: UIImage)
}

class TodayViewControllerDataSource {
    let dependencies: WeekVCDependencies
    var coords: Coordinates?
    weak var feedback: TodayVCDataSourceUpdateFeedback?
    
    init(dependencies: WeekVCDependencies){
        self.dependencies = dependencies
    }
    
    func fetchLocation() {
        coords = dependencies.locationUseCase.fetchCoordinates()
    }
    
    func fetchData() {
        if(coords != nil){
        fetchWeather(lat: (coords?.lat)!, lon: (coords?.lon)!)
        }
    }
    
    private func fetchWeather(lat: Double, lon: Double) {
        dependencies.weatherUseCase.fetchWeatherList(lat: lat, lon: lon) { [weak self] resp in
            switch resp {
            case.success(let weather):
                self?.feedback?.dataSourceUpdated(weather: weather.first!)
                self?.fetchIcon(weather: weather.first!)
            case .error(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    private func fetchIcon(weather: Weather){
        dependencies.iconUseCase.fetchIcon(for: weather.iconPath) {[weak self] response in
            switch response {
            case .success(let icon):
                self?.feedback?.dataSourceImageFetched(image: UIImage(data: icon.image) ?? UIImage())
            case .error(let error):
                print(error.localizedDescription)
            }
        }
    }
}
