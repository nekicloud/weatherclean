//
//  WeatherCell.swift
//  WeatherClean
//
//  Created by Luka on 17/05/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit

class WeatherCell: UITableViewCell {
    @IBOutlet weak var weatherImage: UIImageView!
    @IBOutlet weak var currentTime: UILabel!
    @IBOutlet weak var forecast: UILabel!
    @IBOutlet weak var degrees: UILabel!
}
