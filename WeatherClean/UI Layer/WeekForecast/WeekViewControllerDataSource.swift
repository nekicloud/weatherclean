//
//  WeekVCDependencies.swift
//  WeatherClean
//
//  Created by Luka on 17/05/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit

protocol WeekVCDependencies {
    var weatherUseCase: WeatherUseCase {get}
    var iconUseCase: WeatherIconUseCase {get}
    var locationUseCase: CoordinatesUseCase{get}
}

protocol WeekVCDataSourceUpdateFeedback: class{
    func dataSourceUpdated()
}

class WeekViewControllerDataSource: NSObject {
    let cellID = "weatherCell"
    let dependencies: WeekVCDependencies
    var coords: Coordinates?
    var weatherList = WeatherList()
    var groupedWeatherList = [WeatherList]()
    weak var feedback: WeekVCDataSourceUpdateFeedback?
    let currentDate = Date()
    let dateFormatter = DateFormatter()
    
    
    init(dependencies: WeekVCDependencies){
        self.dependencies=dependencies
    }
    
    func fetchLocation() {
        coords = dependencies.locationUseCase.fetchCoordinates()
    }
    
    func fetchData() {
        if(coords != nil){
            fetchList(lat: (coords?.lat)!, lon: (coords?.lon)!)
        }
    }
    
    private func fetchList(lat: Double, lon: Double){
        dependencies.weatherUseCase.fetchWeatherList(lat: lat, lon: lon) {[unowned self] response in
            switch response{
            case .success(let weatherList):
                self.weatherList = weatherList
                self.groupWeather()
                self.feedback?.dataSourceUpdated()
            case .error(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    private func fetchIcon(for path: String, for cell: WeatherCell) {
        dependencies.iconUseCase.fetchIcon(for: path) { response in
            switch response{
            case .success(let icon):
                let image = UIImage(data:icon.image)
                cell.weatherImage.image = image
            case .error(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    private func hourText(date: Date) -> String{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        formatter.locale = Locale(identifier: "en_US_POSIX")
        let dtTxt = formatter.string(from: date)
        let start = dtTxt.index(dtTxt.startIndex, offsetBy: 11)
        let end = dtTxt.index(dtTxt.endIndex, offsetBy: -3)
        let range = start..<end
        return String(dtTxt[range])
    }
    
    private func groupWeather(){
        if self.weatherList.count > 0 {
            var comparingWeather = weatherList[0]
            self.groupedWeatherList.append([comparingWeather])
            for i in 1 ..< weatherList.count {
                let weather = weatherList[i]
                if Calendar.current.isDate(weather.date, inSameDayAs: comparingWeather.date) {
                    self.groupedWeatherList[self.groupedWeatherList.count - 1].append(weather)
                } else {
                    self.groupedWeatherList.append([weather])
                    comparingWeather = weather
                }
            }
        } else {
            self.groupedWeatherList = []
        }
    }
    
}

extension WeekViewControllerDataSource: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.groupedWeatherList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.groupedWeatherList[section].count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let date = self.groupedWeatherList[section][0].date
        if Calendar.current.isDate(date, inSameDayAs: currentDate) {
            return "Today"
        }
        let title = dateFormatter.weekdaySymbols[Calendar.current.component(.weekday, from: date)-1]
        return title
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath)
        guard let weatherCell = cell as? WeatherCell else { return cell}
        let weather = groupedWeatherList[indexPath.section][indexPath.row]
        weatherCell.currentTime.text = hourText(date: weather.date)
        weatherCell.forecast.text = weather.description
        weatherCell.degrees.text = String(format: "%.0f", ((weather.temperature)-273.15).rounded()) + "°"
        fetchIcon(for: weather.iconPath, for: weatherCell)
        return weatherCell
    }
}
