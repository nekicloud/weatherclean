//
//  WeekViewController.swift
//  WeatherClean
//
//  Created by Luka on 15/05/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit

class WeekViewController: UIViewController{
    @IBOutlet weak var tableView : UITableView!
    let cellID = "weatherCell"
    let dataSource: WeekViewControllerDataSource
    
    init(dataSource:WeekViewControllerDataSource){
        self.dataSource = dataSource
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        return nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        dataSource.fetchLocation()
        dataSource.fetchData()
    }
    
    private func setUpTableView() {
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        tableView.dataSource = dataSource
        dataSource.feedback = self
        tableView.register(UINib(nibName: "WeatherCell", bundle: nil), forCellReuseIdentifier: cellID)
    }
}

extension WeekViewController: WeekVCDataSourceUpdateFeedback {
    func dataSourceUpdated() {
        tableView.reloadData()
    }
}
