//
//  AppCoordinator.swift
//  WeatherClean
//
//  Created by Luka on 15/05/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit

class AppCoordinator{
    let window: UIWindow
    let tabBarController: TabBarController
    
    init(window: UIWindow = UIWindow(frame: UIScreen.main.bounds), dependencies: AppDependenciesContainer){
        self.window = window
        self.tabBarController = TabBarController(dependencies: dependencies)
    }
    
    func start() {
        window.rootViewController = tabBarController
        window.makeKeyAndVisible()
    }
}
