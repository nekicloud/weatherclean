//
//  TabBarController.swift
//  WeatherClean
//
//  Created by Luka on 03/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {
    let todayVC: TodayViewController
    let forecastVC: WeekViewController
    
    init(dependencies: AppDependenciesContainer){
        self.todayVC = TodayViewController(dataSource: TodayViewControllerDataSource(dependencies: dependencies))
        self.forecastVC = WeekViewController(dataSource: WeekViewControllerDataSource(dependencies: dependencies))
        super.init(nibName: nil, bundle: nil)
        setUpTabBarController()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setUpTabBarController(){
        let icon1 = UITabBarItem(title: "Today", image: UIImage(named: "suntab"), tag: 0)
        let icon2 = UITabBarItem(title: "Forecast", image: UIImage(named: "cloudtab"), tag: 1)
        todayVC.tabBarItem = icon1
        forecastVC.tabBarItem = icon2
        let tabBarList = [todayVC,forecastVC]
        self.viewControllers = tabBarList
    }
}
