//
//  AppDependecy.swift
//  WeatherClean
//
//  Created by Luka on 17/05/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

class AppDependenciesContainer: WeekVCDependencies{
    var weatherUseCase: WeatherUseCase = WeatherInteractor(provider: OWAWeatherProvider())
    var iconUseCase: WeatherIconUseCase = WeatherIconInteractor(provider: OWAWeatherIconProvider())
    var locationUseCase:CoordinatesUseCase = CoordinatesInteractor(provider: LocationProvider())
}
